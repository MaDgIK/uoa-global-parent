# uoa-global-parent

This parent contains the global configuration for:

- Maven Repository
- Build configuration for:
  - Maven Release.
  - Maven Compilation.
  - Plugin Management for maven war and jar plugins.
- Distribution Management
- Global dependencies for logging
- Global properties for java and all plugins and dependencies.

## Usage

    <parent>
        <groupId>eu.dnetlib</groupId>
        <artifactId>uoa-global-parent</artifactId>
        <version>2.0.3</version>
    </parent>

